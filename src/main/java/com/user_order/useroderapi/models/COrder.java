package com.user_order.useroderapi.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "order_pizza")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_code", nullable = false, updatable = false)
    private long orderCode;

    @Column(name = "pizza_size", nullable = false, updatable = true)
    private String pizzaSize;
    
    @Column(name = "pizza_type", nullable = false, updatable = true)
    private String pizzaType;

    @Column(name = "voucher_code", nullable = true, updatable = true)
    private String voucherCode;

    @Column(name = "price", nullable = true, updatable = true)
    private long price;

    @Column(name = "paid", nullable = true, updatable = true)
    private long paid;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "created_date")
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedBy
    @Column(name = "updated_date")
    private Date updated;

    @ManyToOne
    @JsonBackReference
    private CUser user;

    public COrder() {
    }

    public COrder(long orderCode, String pizzaSize, String pizzaType, String voucherCode, long price, long paid,
            Date created, Date updated, CUser user) {
        this.orderCode = orderCode;
        this.pizzaSize = pizzaSize;
        this.pizzaType = pizzaType;
        this.voucherCode = voucherCode;
        this.price = price;
        this.paid = paid;
        this.created = created;
        this.updated = updated;
        this.user = user;
    }

    public long getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(long orderCode) {
        this.orderCode = orderCode;
    }

    public String getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public String getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPaid() {
        return paid;
    }

    public void setPaid(long paid) {
        this.paid = paid;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public CUser getUser() {
        return user;
    }

    public void setUser(CUser user) {
        this.user = user;
    }
   
   
}
