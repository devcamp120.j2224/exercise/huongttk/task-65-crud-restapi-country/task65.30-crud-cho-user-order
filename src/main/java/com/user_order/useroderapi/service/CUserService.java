package com.user_order.useroderapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.user_order.useroderapi.models.CUser;
import com.user_order.useroderapi.repository.IUserRepository;

@Service
public class CUserService {
    @Autowired
    //-------------IMPOTR INTERFACE--------------
    IUserRepository userRepository;


    //-------------GET---------------------

    public ResponseEntity <List<CUser>> getAllUser() {
        try {
            List <CUser> listUser = new ArrayList<CUser>();
            userRepository.findAll().forEach(listUser :: add);
            return new ResponseEntity<>(listUser, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity <Object> getUserById(long id) {
        Optional<CUser> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            return new ResponseEntity<>(userData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //-------------------------POST-----------------------

    public ResponseEntity<Object> createUser(CUser user) {
        try {
            Optional<CUser> userData = userRepository.findById(user.getId());
            if (userData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" User already exsit  ");
            }
            user.setCreated(new Date());
            CUser newUser = userRepository.save(user);
            return new ResponseEntity<>(newUser, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
    }

    //------------- PUT--------------------

    public ResponseEntity<Object> updateUserById(long id, CUser user) {
        try {
            Optional<CUser> userData = userRepository.findById(id);

            if (userData.isPresent()) {
                CUser userUpdate = userData.get();
                userUpdate.setFullname(user.getFullname());
                userUpdate.setEmail(user.getEmail());
                userUpdate.setAddress(user.getAddress());
                userUpdate.setPhone(user.getPhone());
                userUpdate.setOrders(user.getOrders());
                userUpdate.setUpdated(new Date());
                return new ResponseEntity<>(userRepository.save(userUpdate), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //////////////////////////// DELETE SERVICES///////////////////////////
    public ResponseEntity<Object> deleteUserById(long id) {
        try {
            userRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteUsers() {
        try {
            userRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
